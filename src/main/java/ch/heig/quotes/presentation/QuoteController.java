package ch.heig.quotes.presentation;

import ch.heig.quotes.model.Quote;
import ch.heig.quotes.service.QuoteDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@RestController
public class QuoteController {

    @Autowired
    private QuoteDAO quoteDAO;

    @RequestMapping(value = "/quotes", method = RequestMethod.GET)
    public List<Quote> listQuotes() {
        return quoteDAO.findAll();
    }

    @GetMapping (value = "/quotes/{id}")
    public Quote ListQuote(@PathVariable int id) {
        return quoteDAO.findById(id);
    }

    @PostMapping(value = "/quotes")
    public ResponseEntity<Void> addQuote(@RequestBody Quote quote) {
        Quote quoteAdded = quoteDAO.save(quote);
        if (quoteAdded == null) {
            return ResponseEntity.noContent().build();
        }
        URI uri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(quote.getId())
                .toUri();
        return ResponseEntity.created(uri).build();
    }

}
