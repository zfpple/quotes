package ch.heig.quotes.service;


import ch.heig.quotes.model.Quote;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class QuoteDAO {
    private List<Quote> quotes;

    public QuoteDAO (){
        quotes= new ArrayList<>();
        quotes.add(new Quote(1,"Dr. Seuss", "Don't cry because it's over, smile because it happened."));
        quotes.add(new Quote(2,"Oscar Wilde", "Be yourself; everyone else is already taken."));
        quotes.add(new Quote(3,"Albert Einstein", "Two things are infinite: the universe and human stupidity; and I'm not sure about the universe."));
    }

    public List<Quote> findAll() {
        return quotes;
    }

    public Quote findById(int id) {
        for (Quote quote : quotes) {
            if (quote.getId()==id) {
                return quote;
            }
        }
        return null;
    }

    public Quote save(Quote quote) {
        quotes.add(quote);
        return quote;
    }


}
